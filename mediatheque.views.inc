<?php

/**
 * Implementation of hook_views_data().
 */
function mediatheque_views_data() {
  $data['mediatheque_log']['table']['group'] = t('Mediatheque');

  $data['mediatheque_log']['table']['base'] = array(
    'field' => 'mlid',
    'title' => t('Mediatheque log'),
    'help' => t('Mediatheque log messages.'),
  );

  $data['mediatheque_log']['table']['join'] = array(
    'mediatheque_volumes' => array(
      'left_field' => 'mvid',
      'field' => 'mvid',
      'type' => 'INNER',
    ),
  );

  $data['mediatheque_log']['mlid'] = array(
    'title' => t('Log id'),
    'help' => t('The primary identifier of the log message.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
      'numeric' => TRUE,
    ),
  );

  $data['mediatheque_log']['mvid'] = array(
    'title' => t('Volume id'),
    'help' => t('The primary identifier of the volume containing the document being processed.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'mediatheque_handler_filter_mvid',
      'parent' => 'views_handler_filter_in_operator',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'mediatheque_handler_argument_mvid',
      'parent' => 'views_handler_argument_numeric',
      'numeric' => TRUE,
    ),
  );

  $data['mediatheque_log']['nid'] = array(
    'title' => t('Node id'),
    'help' => t('Relate a log to the document being processed.'),
    'relationship' => array(
      'handler' => 'views_handler_relationship',
      'base' => 'node',
      'base field' => 'nid',
      'label' => t('node'),
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_node_nid',
      'parent' => 'views_handler_argument_numeric', // make sure parent is included
      'numeric' => TRUE,
      'validate type' => 'nid',
    ),
  );

  $data['mediatheque_log']['timestamp'] = array(
    'title' => t('Timestamp'),
    'help' => t('The timestamp of the log entry.'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_date',
    ),
  );

  $data['mediatheque_log']['plugin'] = array(
    'title' => t('Plugin'),
    'help' => t('The plugin that was applied to the document.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_in_operator',
      'options callback' => '_mediatheque_get_plugins',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  $data['mediatheque_log']['version'] = array(
    'title' => t('Plugin version'),
    'help' => t('The version of the plugin that was applied to the document.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  $data['mediatheque_log']['status'] = array(
    'title' => t('Status'),
    'help' => t('The return status of this log.'),
    'field' => array(
      'handler' => 'views_handler_field_boolean',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_boolean_operator',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
  );

  $data['mediatheque_log']['log'] = array(
    'title' => t('Log message'),
    'help' => t('The log message.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  $data['mediatheque_log']['md5'] = array(
    'title' => t('File hash'),
    'help' => t('The MD5 signature of the file that was processed.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  $data['mediatheque_volumes']['table']['group'] = t('Mediatheque');

  $data['mediatheque_volumes']['table']['join'] = array(
    'mediatheque_log' => array(
      'left_field' => 'mvid',
      'field' => 'mvid',
      'type' => 'INNER',
    ),
    'mediatheque_nodes_volumes' => array(
      'left_field' => 'mvid',
      'field' => 'mvid',
    ),
    'node' => array(
      'left_table' => 'mediatheque_nodes_volumes',
      'left_field' => 'mvid',
      'field' => 'mvid',
    ),
  );

  $data['mediatheque_volumes']['mvid'] = array(
    'title' => t('Volume id'),
    'help' => t('The primary identifier of the volume.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'mediatheque_handler_filter_mvid',
      'parent' => 'views_handler_filter_in_operator',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'mediatheque_handler_argument_mvid',
      'handler' => 'views_handler_argument_numeric',
      'numeric' => TRUE,
    ),
  );

  $data['mediatheque_volumes']['label'] = array(
    'title' => t('Volume label'),
    'help' => t('The human-readable volume label.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  $data['mediatheque_volumes']['path'] = array(
    'title' => t('Volume path'),
    'help' => t('The volume root path.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  $data['mediatheque_plugins']['table']['group'] = t('Mediatheque');

  $data['mediatheque_plugins']['table']['join'] = array(
    'mediatheque_log' => array(
      'left_field' => 'plugin',
      'field' => 'plugin',
      'type' => 'INNER',
    ),
  );

  $data['mediatheque_plugins']['mpid'] = array(
    'title' => t('Plugin id'),
    'help' => t('The primary identifier of the plugin.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
      'numeric' => TRUE,
    ),
  );

  $data['mediatheque_plugins']['mimetype'] = array(
    'title' => t('MIME types'),
    'help' => t('The MIME type patterns supported by this plugin.'),
    'field' => array(
      'handler' => 'views_handler_field',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
  );

  $data['mediatheque_log']['active'] = array(
    'title' => t('Active'),
    'help' => t('Whether this plugin is enabled or disabled.'),
    'field' => array(
      'handler' => 'views_handler_field_boolean',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_boolean_operator',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
  );

  $data['mediatheque_plugins']['weight'] = array(
    'title' => t('Weight'),
    'help' => t('The plugin processing order.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
  );

  $data['mediatheque_nodes_volumes']['table']['group'] = t('Mediatheque');

  $data['mediatheque_nodes_volumes']['table']['join'] = array(
    'node' => array(
      'left_field' => 'nid',
      'field' => 'nid',
    ),
    'mediatheque_volumes' => array(
      'left_field' => 'mvid',
      'field' => 'mvid',
    ),
  );

  $data['mediatheque_nodes_volumes']['nid'] = array(
    'title' => t('Node'),
    'relationship' => array(
      'handler' => 'views_handler_relationship',
      'base' => 'node',
      'base field' => 'nid',
      'label' => t('node'),
    ),
  );

  return $data;
}

function mediatheque_views_data_alter(&$data) {
}

/**
 * Implementation of hook_views_handlers().
 */
function mediatheque_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'mediatheque') . '/views',
    ),
    'handlers' => array(
      'mediatheque_handler_argument_mvid' => array(
        'parent' => 'views_handler_argument_numeric',
      ),
      'mediatheque_handler_filter_mvid' => array(
        'parent' => 'views_handler_filter_vocabulary_vid',
      ),
    ),
  );
}

/**
 * Helper function to return active plugins for plugin filter handler.
 */
function _mediatheque_get_plugins() {
  $plugins = array('MediathequeCore' => 'MediathequeCore');
  $result = db_query("SELECT plugin FROM {mediatheque_plugins} WHERE active=1");
  while ($plugin = db_fetch_object($result)) {
    $plugins[$plugin->plugin] = $plugin->plugin;
  }
  return $plugins;
}

