<?php

class MediathequeVideoStreamPlugin extends MediathequePreviewPlugin {
  function render() {
    $video = url('node/' . $this->doc->nid . '/mediatheque/download');
    if (empty($video)) {
      drupal_set_message(t('Error previewing file.'), 'error', FALSE);
      return '';
    }
    $type = $this->doc->field_metadata[MEDIATHEQUE_MIMETYPE]['value'];
    return <<<EOS
      <video controls autoplay>
        <source src="$video" type="$type" />
      </video>
EOS;
  }
}
