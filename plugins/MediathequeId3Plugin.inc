<?php

class MediathequeId3Plugin extends MediathequeMetadataPlugin {

  static function getPrefix() {
    return 'id3';
  }

  static function getVersion() {
    return '1.3';
  }

  function getMetadata($force) {
    if (!empty($this->metadata) && !$force) {
      return $this->metadata;
    }
    include_once('sites/all/libraries/getid3/getid3/getid3.php');
    $getID3 = new getID3;
    $getID3->option_md5_data = TRUE;
    $getID3->option_md5_data_source = TRUE;
    $getID3->encoding = 'UTF-8';
    $id3 = $getID3->analyze($this->path);
    if (!empty($id3['tags'])) foreach ($id3['tags'] as $tag_name => $tag_values) {
      if ($tag_name === 'id3v1') continue; // discard id3v1
      foreach ($tag_values as $tag_key => $tag_array) {
        if (count($tag_array) == 1) {
          $this->metadata[] = array(
            'name' => $tag_key,
            'value' => $tag_array[0],
          );
        }
        else {
          foreach ($tag_array as $tag_index => $tag_value) {
            $this->metadata[] = array(
              'name' => $tag_key . ':' . $tag_index,
              'value' => $tag_value,
            );
          }
          $this->metadata[] = array(
            'name' => $tag_key,
            'value' => implode(' - ', $tag_array),
          );
        }
      }
    }
    if (!empty($id3['audio'])) foreach ($id3['audio'] as $tag_name => $tag_value) {
      if ($tag_name === 'streams') continue;
      $this->metadata[] = array(
        'name' => $tag_name,
        'value' => $tag_value,
      );
    }
    if (!empty($id3['video'])) foreach ($id3['video'] as $tag_name => $tag_value) {
      if ($tag_name === 'streams') continue;
      $this->metadata[] = array(
        'name' => $tag_name,
        'value' => $tag_value,
      );
    }

    // Generate thumbnails if needed.
    // For images: generate a thumbnail or point to original image.
    // For embedded ID3 pictures: extract image data and resize.
    // For audio: look for separate thumbnails in same folder and resize.
    $type = mediatheque_get_media_type($id3['mime_type']);
    if ($type == 'image') {
      if (!$this->makeThumbnailFromFile($this->path, $force)) {
        $url = url(mediatheque_get_download_path($this->doc));
        $this->metadata[] = array(
          'name' => 'thumbnail',
          'value' => $url,
        );
      }
    }
    else if (!empty($id3['comments']['picture'])) foreach ($id3['comments']['picture'] as $picture) {
      if ($this->makeThumbnailFromID3($picture, $force)) break;
    }
    else if ($type == 'audio') {
      // Go up the folders looking for the thumbnail file.
      $vol = mediatheque_volume_load($this->doc->mediatheque['mvid']);
      $pathinfo = pathinfo($this->path);
      $dirs = explode(DIRECTORY_SEPARATOR, $pathinfo['dirname']);
      foreach ($dirs as $i => $dir) {
        // @see https://stackoverflow.com/questions/6290146/how-to-split-a-path-properly-in-php
        $dirname = implode(DIRECTORY_SEPARATOR, array_slice($dirs, 0, count($dirs) - $i));
        if ($dirname === $vol->path) break; // don't check volume root or above
        $dirname = preg_replace('#[\[\]\?\s]#', '\\\${0}', $dirname);
        $glob = "$dirname/{" . variable_get('mediatheque_folder_files', 'Folder,folder') . '}.*';
        foreach (glob($glob, GLOB_BRACE) as $filename) {
          if ($this->makeThumbnailFromFile($filename, $force)) break 2;
        }
      }
    }
    return $this->metadata;
  }

  function makeThumbnailFromID3($picture, $force) {
    $folder = file_directory_path() . '/mediatheque/id3';
    if (!is_dir($folder) && !@mkdir($folder, 0777, TRUE)) {
      $this->status = FALSE;
      $this->permanent = TRUE;
      $this->log = t('Error creating path "!path". Aborted cover extraction.', array(
        '!path' => $folder,
      ));
      return FALSE;
    }
    $extension = mediatheque_get_media_extension($picture['image_mime']);
    $cover = $folder . '/' . $this->doc->field_metadata[MEDIATHEQUE_MD5]['value'] . $extension;
    if (file_exists($cover)) {
      if ($force) {
        unlink($cover);
      }
      else {
        $this->metadata[] = array('name' => 'thumbnail', 'value' => url($cover));
        return TRUE;
      }
    }
    $cover = file_create_filename($this->doc->field_metadata[MEDIATHEQUE_MD5]['value'] . $extension, $folder);
    if (FALSE !== file_put_contents($cover, $picture['data'])) {
      list(, $output, $return) = mediatheque_exec('mogrify -resize %1 %2',
        variable_get('mediatheque_thumbnail_width', 128), $cover
      );
      if ($return === 0) {
        $this->metadata[] = array('name' => 'thumbnail', 'value' => url($cover));
      }
      else {
        $this->status = FALSE;
        $this->permanent = FALSE;
        $this->log = t('Error resizing thumbnail at "!path": !error', array(
          '!path' => $cover,
          '!error' => $output,
        ));
        return FALSE;
      }
    }
    else {
      $this->status = FALSE;
      $this->permanent = FALSE;
      $this->log = t('Error writing thumbnail to "!path"', array(
        '!path' => $cover,
      ));
      return FALSE;
    }
    return TRUE;
  }

  function makeThumbnailFromFile($filename, $force) {
    $folder = file_directory_path() . '/mediatheque/id3';
    if (!is_dir($folder) && !@mkdir($folder, 0777, TRUE)) {
      $this->status = FALSE;
      $this->permanent = TRUE;
      $this->log = t('Error creating path "!path". Aborted cover extraction.', array(
        '!path' => $folder,
      ));
      return FALSE;
    }
    $mimetype = mediatheque_get_mimetype($filename);
    $extension = mediatheque_get_media_extension($mimetype);
    $cover = $folder . '/' . $this->doc->field_metadata[MEDIATHEQUE_MD5]['value'] . $extension;
    if (file_exists($cover)) {
      if ($force) {
        unlink($cover);
      }
      else {
        $this->metadata[] = array('name' => 'thumbnail', 'value' => url($cover));
        return TRUE;
      }
    }
    $cover = file_create_filename($this->doc->field_metadata[MEDIATHEQUE_MD5]['value'] . $extension, $folder);
    list(, $output, $return) = mediatheque_exec('convert -resize %1 %2 %3',
      variable_get('mediatheque_thumbnail_width', 128), $filename, $cover
    );
    if ($return === 0) {
      $this->metadata[] = array('name' => 'thumbnail', 'value' => url($cover));
    }
    else {
      $this->status = FALSE;
      $this->permanent = FALSE;
      $this->log = t('Error resizing thumbnail at "!path": !error', array(
        '!path' => $cover,
        '!error' => $output,
      ));
      return FALSE;
    }
    return TRUE;
  }
}
