<?php

class MediathequeIsbnPlugin extends MediathequeMetadataPlugin {

  static function getPrefix() {
    return 'isbn';
  }

  static function getVersion() {
    return '1.4';
  }

  static function getKeys() {
    return array(self::getPrefix() . ':identifier');
  }

  function getMetadata($force) {
    if (!empty($this->metadata) && !$force) {
      return $this->metadata;
    }

    // Query ISBN databases using Google Books API.
    $isbn = trim(pathinfo($this->path, PATHINFO_FILENAME));
    if (preg_match('/^(97(8|9))?\d{9}(\d|X)$/', $isbn, $matches)) {
      if ($this->getMetadataAmazon($isbn)) {
        // done Amazon
      }
      else if ($this->getMetadataGoogleBooks($isbn)) {
        // done Google Books
      }
      else {
        // get the mininum metadata we can extract ourselves.
        $this->metadata[] = array(
          'name' => 'identifier',
          'value' => 'ISBN_' . strlen($isbn) . ':' . $isbn
        );
      }
    }
    else if (preg_match('/^[A-Z0-9]{10}$/', $isbn, $matches)) {
      $this->metadata[] = array(
        'name' => 'identifier',
        'value' => 'ASIN:' . $isbn
      );
    }
    else {
      $this->status = FALSE;
      $this->permanent = TRUE;
      $this->log = t('Filename !filename is not a known book identifier.', array('!filename' => $path));
    }

    // Get thumbnail if missing.
    if (!$this->getThumbnails()) {
      $this->makeThumbnail($force);
    }

    return $this->metadata;
  }

  function getMetadataAmazon($isbn) {
    $key = variable_get('mediatheque_amazon_api_key', '');
    if (empty($key)) {
      watchdog('mediatheque', 'Amazon API key not found. Please set the variable `mediatheque_amazon_api_key`.', array(), WATCHDOG_WARNING);
      return FALSE;
    }
    $secret = variable_get('mediatheque_amazon_api_secret', '');
    if (empty($secret)) {
      watchdog('mediatheque', 'Amazon API secret not found. Please set the variable `mediatheque_amazon_api_secret`.', array(), WATCHDOG_WARNING);
      return FALSE;
    }
    $associate_id = variable_get('mediatheque_amazon_associate_id', '');
    if (empty($associate_id)) {
      watchdog('mediatheque', 'Amazon Associate Tag not found. Please set the variable `mediatheque_amazon_associate_id`.', array(), WATCHDOG_WARNING);
      return FALSE;
    }
    $params = array(
      'Service' => 'AWSECommerceService',
      'Operation' => 'ItemLookup',
      'ResponseGroup' => 'ItemAttributes,Images,EditorialReview',
      'SearchIndex' => 'All',
      'IdType' => 'ISBN',
      'ItemId' => $isbn,
      'AWSAccessKeyId' => $key,
      'AssociateTag' => $associate_id,
      'Timestamp' => gmdate("Y-m-d\TH:i:s\Z"),
      'Version' => '2011-08-01',
    );
    ksort($params);
    $params['Signature'] = base64_encode(hash_hmac(
      'sha256',
      "GET\nwebservices.amazon.com\n/onca/xml\n" . $this->buildQueryString($params),
      $secret,
      TRUE
    ));
    $result = drupal_http_request(
      url(
        'https://webservices.amazon.com/onca/xml',
        array(
          'external' => TRUE,
          'query' => $params,
        )
      )
    );
    if ($result->code != 200) {
      // TODO watchdog
      return FALSE;
    }
    $item = json_decode(json_encode((array) simplexml_load_string($result->data)), 1);
    if (empty($item['Items']['Item'])) {
      // TODO watchdog
      return FALSE;
    }
    $book = array_key_exists('ASIN', $item['Items']['Item']) ?
      $item['Items']['Item'] :
      $item['Items']['Item'][0]
    ;
    if (!empty($book['ItemAttributes']['Title'])) {
      $this->metadata[] = array('name' => 'title', 'value' => $book['ItemAttributes']['Title']);
    }
    if (!empty($book['ItemAttributes']['PublicationDate'])) {
      $this->metadata[] = array('name' => 'date', 'value' => $book['ItemAttributes']['PublicationDate']);
    }
    if (!empty($book['ItemAttributes']['Author'])) foreach ((array)$book['ItemAttributes']['Author'] as $author) {
      $this->metadata[] = array('name' => 'creator', 'value' => $author);
    }
    if (!empty($book['ItemAttributes']['Creator'])) foreach ((array)$book['ItemAttributes']['Creator'] as $creator) {
      $this->metadata[] = array('name' => 'creator', 'value' => $creator);
    }
    if (!empty($book['ItemAttributes']['Publisher'])) {
      $this->metadata[] = array('name' => 'publisher', 'value' => $book['ItemAttributes']['Publisher']);
    }
    if (!empty($book['ItemAttributes']['ASIN'])) {
      $this->metadata[] = array('name' => 'identifier', 'value' => 'ASIN:' . $book['ItemAttributes']['ASIN']);
    }
    if (!empty($book['ItemAttributes']['MPN'])) {
      $this->metadata[] = array('name' => 'identifier', 'value' => 'MPN:' . $book['ItemAttributes']['MPN']);
    }
    if (!empty($book['ItemAttributes']['ISBN'])) {
      $this->metadata[] = array(
        'name' => 'identifier',
        'value' => 'ISBN_' . (strlen($book['ItemAttributes']['ISBN'])) . ':' . $book['ItemAttributes']['ISBN']
      );
    }
    if (!empty($book['ItemAttributes']['EANList'])) foreach ((array)$book['ItemAttributes']['EANList'] as $identifier) {
      $this->metadata[] = array('name' => 'identifier', 'value' => 'EAN:' . $identifier);
    }
    if (!empty($book['ItemAttributes']['ProductGroup'])) {
      $this->metadata[] = array('name' => 'format', 'value' => $book['ItemAttributes']['ProductGroup']);
    }
    if (!empty($book['ItemAttributes']['NumberOfPages'])) {
      $this->metadata[] = array('name' => 'format', 'value' => $book['ItemAttributes']['NumberOfPages'] . ' pages');
    }
    if (!empty($book['ItemAttributes']['Languages']['Language'][0])) {
      $this->metadata[] = array(
        'name' => 'language',
        'value' => mediatheque_get_language_code($book['ItemAttributes']['Languages']['Language'][0]['Name'])
      );
    }
    if (!empty($book['EditorialReviews']['EditorialReview'])) {
      $this->metadata[] = array(
        'name' => 'description',
        'value' => array_key_exists('Content', $book['EditorialReviews']['EditorialReview']) ?
          $book['EditorialReviews']['EditorialReview']['Content'] :
          $book['EditorialReviews']['EditorialReview'][0]['Content']
      );
    }
    foreach (array('MediumImage', 'SmallImage', 'LargeImage') as $image) {
      if (!empty($book[$image]['URL'])) {
        $this->metadata[] = array(
          'name' => 'thumbnail',
          'value' => $book[$image]['URL']
        );
      }
    }
    return TRUE;
  }

  protected function buildQueryString(array $params) {
    $parameterList = array();
    foreach ($params as $key => $value) {
      $parameterList[] = sprintf('%s=%s', $key, rawurlencode($value));
    }
    return implode("&", $parameterList);
  }

  function getMetadataGoogleBooks($isbn) {
    $key = variable_get('mediatheque_google_api_key', '');
    if (empty($key)) {
      watchdog('mediatheque', 'Google API key not found. Please set the variable `mediatheque_google_api_key`.', array(), WATCHDOG_WARNING);
      return FALSE;
    }
    $result = drupal_http_request(
      url(
        'https://www.googleapis.com/books/v1/volumes',
        array(
          'external' => TRUE,
          'query' =>  array(
            'q' => 'isbn:' . $isbn,
            'key' => $key,
          ),
        )
      )
    );
    $data = json_decode($result->data);
    if ($result->code != 200) {
      $this->status = FALSE;
      $this->permanent = FALSE;
      $this->log = t('!code error fetching metadata from Google Books for !isbn: !error', array(
        '!code' => $data->error->code,
        '!isbn' => $isbn,
        '!error' => $data->error->message,
      ));
      return FALSE;
    }
    if (!empty($data->items)) foreach ($data->items as $item) {
      $book = $item->volumeInfo;
      // title
      if (!empty($book->title)) {
        $this->metadata[] = array('name' => 'title', 'value' => $book->title);
      }
      // subtitle
      if (!empty($book->subtitle)) {
        $this->metadata[] = array('name' => 'title', 'value' => $book->subtitle);
      }
      // authors
      if (is_array($book->authors)) foreach ($book->authors as $author) {
        $this->metadata[] = array('name' => 'creator', 'value' => $author);
      }
      // publisher
      if (!empty($book->publisher)) {
        $this->metadata[] = array('name' => 'publisher', 'value' => $book->publisher);
      }
      // publishedDate
      if (!empty($book->publishedDate)) {
        $this->metadata[] = array('name' => 'date', 'value' => $book->publishedDate);
      }
      // description
      if (!empty($book->description)) {
        $this->metadata[] = array('name' => 'description', 'value' => $book->description);
      }
      // identifiers
      if (!empty($item->id)) {
        $this->metadata[] = array('name' => 'identifier', 'value' => $item->id);
      }
      if (is_array($book->industryIdentifiers)) foreach ($book->industryIdentifiers as $identifier) {
        $this->metadata[] = array('name' => 'identifier', 'value' => $identifier->type . ':' . $identifier->identifier);
      }
      // pageCount
      if (!empty($book->pageCount)) {
        $this->metadata[] = array('name' => 'format', 'value' => $book->pageCount . ' pages');
      }
      // printType
      if (!empty($book->printType)) {
        $this->metadata[] = array('name' => 'format', 'value' => mb_strtolower($book->printType));
      }
      // categories
      if (is_array($book->categories)) foreach ($book->categories as $category) {
        $this->metadata[] = array('name' => 'topic', 'value' => $category);
      }
      // thumbnails
      if (is_object($book->imageLinks)) {
        $this->metadata[] = array('name' => 'thumbnail', 'value' => $book->imageLinks->smallThumbnail);
        $this->metadata[] = array('name' => 'thumbnail', 'value' => $book->imageLinks->thumbnail);
        $this->thumbnails[] = $book->imageLinks->smallThumbnail;
      }
      // language
      if (!empty($book->language)) {
        $this->metadata[] = array('name' => 'language', 'value' => $book->language);
      }
    }
    return !empty($this->metadata);
  }

  function makeThumbnail($force) {
    $folder = file_directory_path() . '/mediatheque/isbn';
    if (!is_dir($folder) && !@mkdir($folder, 0777, TRUE)) {
      $this->status = FALSE;
      $this->permanent = TRUE;
      $this->log = t('Error creating path "!path". Aborted cover extraction.', array(
        '!path' => $folder,
      ));
      return FALSE;
    }
    $cover = $folder . '/' . $this->doc->field_metadata[MEDIATHEQUE_MD5]['value'] . '.jpg';
    if (file_exists($cover)) {
      if ($force) {
        unlink($cover);
      }
      else {
        $this->metadata[] = array('name' => 'thumbnail', 'value' => url($cover));
        return TRUE;
      }
    }
    $cover = file_create_filename($this->doc->field_metadata[MEDIATHEQUE_MD5]['value'] . '.jpg', $folder);
    list(, $output, $return) = mediatheque_exec('convert %1[0] -flatten -resize %2 %3',
      $this->path, variable_get('mediatheque_thumbnail_width', 128), $cover
    );
    if ($return === 0) {
      $this->metadata[] = array('name' => 'thumbnail', 'value' => url($cover));
    }
    else {
      // Sometimes `convert` fails. Try alternative approach with `pdftk`.
      $front = file_create_filename('front.pdf', file_directory_temp());
      list(, $output, $return) = mediatheque_exec('pdftk %1 cat 1 output %2',
        $this->path, $front
      );
      if ($return !== 0) {
        $this->status = FALSE;
        $this->permanent = FALSE;
        $this->log = t('Error extracting front page for "!path": !error', array(
          '!path' => $this->path,
          '!error' => $output,
        ));
        return FALSE;
      }
      list(, $output, $return) = mediatheque_exec('convert %1 -flatten -resize %2 %3',
        $front, variable_get('mediatheque_thumbnail_width', 128), $cover
      );
      if ($return === 0) {
        $this->metadata[] = array('name' => 'thumbnail', 'value' => url($cover));
      }
      else {
        $this->status = FALSE;
        $this->permanent = FALSE;
        $this->log = t('Error extracting cover for "!path": !error', array(
          '!path' => $this->path,
          '!error' => $output,
        ));
        return FALSE;
      }
    }
    return TRUE;
  }
}
