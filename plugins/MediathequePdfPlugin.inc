<?php

class MediathequePdfPlugin extends MediathequePreviewPlugin {
  function render() {
    drupal_goto(
      'sites/all/libraries/pdf.js/web/viewer.html',
      array(
        'file' => url('node/' . $this->doc->nid . '/mediatheque/download'),
      )
    );
  }
}
