<?php

class MediathequeAudioStreamPlugin extends MediathequePreviewPlugin {
  function render() {
    $audio = $this->transcode();
    if (empty($audio)) {
      drupal_set_message(t('Error previewing file.'), 'error', FALSE);
      return '';
    }
    return <<<EOS
      <audio controls autoplay>
        <source src="$audio" type="audio/mpeg" />
      </audio>
EOS;
  }

  function transcode() {
    $folder = file_directory_path() . '/mediatheque/audio';
    if (!is_dir($folder) && !@mkdir($folder, 0777, TRUE)) {
      watchdog('mediatheque', 'Error creating path "!path". Aborted audio transcoding.', array(
        '!path' => $folder,
      ), WATCHDOG_ERROR);
      return FALSE;
    }
    $audio = $folder . '/' . $this->doc->field_metadata[MEDIATHEQUE_MD5]['value'] . '.mp3';
    if (file_exists($audio)) {
      return url($audio);
    }
    if (pathinfo($this->path, PATHINFO_EXTENSION) === 'mp3') {
      return url('node/' . $this->doc->nid . '/mediatheque/download');
    }
    $audio = file_create_filename($this->doc->field_metadata[MEDIATHEQUE_MD5]['value'] . '.mp3', $folder);
    list(, $output, $return) = mediatheque_exec('avconv -i %1 -vn %2', $this->path, $audio);
    if ($return !== 0) {
      watchdog('mediatheque', 'Error transcoding audio "!path": !error', array(
        '!path' => $this->path,
        '!error' => $output,
      ), WATCHDOG_ERROR);
      return FALSE;
    }
    return url($audio);
  }
}
