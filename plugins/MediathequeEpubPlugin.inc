<?php

class MediathequeEpubPlugin extends MediathequePreviewPlugin {
  function render() {
    drupal_goto(
      'sites/all/libraries/epub.js/reader/index.html',
      array(
        'bookPath' => url('node/' . $this->doc->nid . '/mediatheque/download/dummy.epub'),
      )
    );
  }
}
