<?php

/**
 * @file
 * Mediatheque API
 */

define('MEDIATHEQUE_MIMETYPE', 0);
define('MEDIATHEQUE_MD5', 1);
define('MEDIATHEQUE_SIZE', 2);
define('MEDIATHEQUE_TYPE', 3);

define('MEDIATHEQUE_CORE', 'MediathequeCore');
define('MEDIATHEQUE_CORE_VERSION', '1.2');

/**
 * Get the mediatheque document content type.
 */
function mediatheque_get_document_type() {
  return variable_get('mediatheque_document_type', 'document');
}

/**
 * Execute a system command.
 */
function mediatheque_exec($cmd) {
  setlocale(LC_CTYPE, "en_US.UTF-8");
  $replacements = array();
  foreach (func_get_args() as $i => $arg) {
    if (!$i) continue;
    $replacements["%$i"] = escapeshellarg($arg);
  }
  $cmd = strtr($cmd, $replacements);
  $cmd = $cmd . ' 2>&1';
  $result = exec($cmd, $output, $return);
  $output = implode("\n", $output);
  return array($result, $output, $return);
}

/**
 * Determine file's MIME type.
 */
function mediatheque_get_mimetype($path) {
  list($mimetype, $output, $return) = mediatheque_exec('file -b --mime-type %1', $path);
  if ($return !== 0) {
    watchdog(
      'mediatheque',
      'Error fetching MIME type for %path: !output',
      array(
        '%path' => $path,
        '!output' => $output
      ),
      WATCHDOG_ERROR
    );
    return 'application/unknown';
  }
  if ($mimetype === 'application/octet-stream') {
    $mimetype = mediatheque_get_mimetype_map(pathinfo($path, PATHINFO_EXTENSION));
  }

  drupal_alter('mediatheque_mimetype', $mimetype, $path);
  return $mimetype;
}

/**
 * Get media type given MIME type.
 **/
function mediatheque_get_media_type($mimetype) {
    list($type, $subtype) = explode('/', $mimetype);

  // TODO use hook/admin interface for these settings.
  switch ($mimetype) {
    case 'application/pdf':
    case 'image/vnd.djvu':
    case 'application/epub+zip':
      return 'ebook';
  }
  if (strpos($mimetype, 'ebook') !== FALSE) {
    return 'ebook';
  }

  return $type;
}

/**
 * Get MIME type given extension.
 */
function mediatheque_get_mimetype_map($extension = NULL, $all = FALSE) {
  // @see https://github.com/bcit-ci/CodeIgniter/blob/develop/application/config/mimes.php
  $map = array(
    'hqx' =>  array('application/mac-binhex40', 'application/mac-binhex', 'application/x-binhex40', 'application/x-mac-binhex40'),
    'cpt' =>  'application/mac-compactpro',
    'csv' =>  array('text/x-comma-separated-values', 'text/comma-separated-values', 'application/octet-stream', 'application/vnd.ms-excel', 'application/x-csv', 'text/x-csv', 'text/csv', 'application/csv', 'application/excel', 'application/vnd.msexcel', 'text/plain'),
    'bin' =>  array('application/macbinary', 'application/mac-binary', 'application/octet-stream', 'application/x-binary', 'application/x-macbinary'),
    'dms' =>  'application/octet-stream',
    'lha' =>  'application/octet-stream',
    'lzh' =>  'application/octet-stream',
    'exe' =>  array('application/octet-stream', 'application/x-msdownload'),
    'class' =>  'application/octet-stream',
    'psd' =>  array('application/x-photoshop', 'image/vnd.adobe.photoshop'),
    'so'  =>  'application/octet-stream',
    'sea' =>  'application/octet-stream',
    'dll' =>  'application/octet-stream',
    'oda' =>  'application/oda',
    'pdf' =>  array('application/pdf', 'application/force-download', 'application/x-download', 'binary/octet-stream'),
    'ai'  =>  array('application/pdf', 'application/postscript'),
    'eps' =>  'application/postscript',
    'ps'  =>  'application/postscript',
    'smi' =>  'application/smil',
    'smil'  =>  'application/smil',
    'mif' =>  'application/vnd.mif',
    'xls' =>  array('application/vnd.ms-excel', 'application/msexcel', 'application/x-msexcel', 'application/x-ms-excel', 'application/x-excel', 'application/x-dos_ms_excel', 'application/xls', 'application/x-xls', 'application/excel', 'application/download', 'application/vnd.ms-office', 'application/msword'),
    'ppt' =>  array('application/powerpoint', 'application/vnd.ms-powerpoint', 'application/vnd.ms-office', 'application/msword'),
    'pptx'  =>  array('application/vnd.openxmlformats-officedocument.presentationml.presentation', 'application/x-zip', 'application/zip'),
    'wbxml' =>  'application/wbxml',
    'wmlc'  =>  'application/wmlc',
    'dcr' =>  'application/x-director',
    'dir' =>  'application/x-director',
    'dxr' =>  'application/x-director',
    'dvi' =>  'application/x-dvi',
    'gtar'  =>  'application/x-gtar',
    'gz'  =>  'application/x-gzip',
    'gzip'  =>  'application/x-gzip',
    'php' =>  array('application/x-httpd-php', 'application/php', 'application/x-php', 'text/php', 'text/x-php', 'application/x-httpd-php-source'),
    'php4'  =>  'application/x-httpd-php',
    'php3'  =>  'application/x-httpd-php',
    'phtml' =>  'application/x-httpd-php',
    'phps'  =>  'application/x-httpd-php-source',
    'js'  =>  array('application/x-javascript', 'text/plain'),
    'swf' =>  'application/x-shockwave-flash',
    'sit' =>  'application/x-stuffit',
    'tar' =>  'application/x-tar',
    'tgz' =>  array('application/x-tar', 'application/x-gzip-compressed'),
    'z' =>  'application/x-compress',
    'xhtml' =>  'application/xhtml+xml',
    'xht' =>  'application/xhtml+xml',
    'zip' =>  array('application/x-zip', 'application/zip', 'application/x-zip-compressed', 'application/s-compressed', 'multipart/x-zip'),
    'rar' =>  array('application/x-rar', 'application/rar', 'application/x-rar-compressed'),
    'mid' =>  'audio/midi',
    'midi'  =>  'audio/midi',
    'mpga'  =>  'audio/mpeg',
    'mp2' =>  'audio/mpeg',
    'mp3' =>  array('audio/mpeg', 'audio/mpg', 'audio/mpeg3', 'audio/mp3'),
    'aif' =>  array('audio/x-aiff', 'audio/aiff'),
    'aiff'  =>  array('audio/x-aiff', 'audio/aiff'),
    'aifc'  =>  'audio/x-aiff',
    'ram' =>  'audio/x-pn-realaudio',
    'rm'  =>  'audio/x-pn-realaudio',
    'rpm' =>  'audio/x-pn-realaudio-plugin',
    'ra'  =>  'audio/x-realaudio',
    'rv'  =>  'video/vnd.rn-realvideo',
    'wav' =>  array('audio/x-wav', 'audio/wave', 'audio/wav'),
    'bmp' =>  array('image/bmp', 'image/x-bmp', 'image/x-bitmap', 'image/x-xbitmap', 'image/x-win-bitmap', 'image/x-windows-bmp', 'image/ms-bmp', 'image/x-ms-bmp', 'application/bmp', 'application/x-bmp', 'application/x-win-bitmap'),
    'gif' =>  'image/gif',
    'jpeg'  =>  array('image/jpeg', 'image/pjpeg'),
    'jpg' =>  array('image/jpeg', 'image/pjpeg'),
    'jpe' =>  array('image/jpeg', 'image/pjpeg'),
    'png' =>  array('image/png',  'image/x-png'),
    'tiff'  =>  'image/tiff',
    'tif' =>  'image/tiff',
    'css' =>  array('text/css', 'text/plain'),
    'html'  =>  array('text/html', 'text/plain'),
    'htm' =>  array('text/html', 'text/plain'),
    'shtml' =>  array('text/html', 'text/plain'),
    'txt' =>  'text/plain',
    'text'  =>  'text/plain',
    'log' =>  array('text/plain', 'text/x-log'),
    'rtx' =>  'text/richtext',
    'rtf' =>  'text/rtf',
    'xml' =>  array('application/xml', 'text/xml', 'text/plain'),
    'xsl' =>  array('application/xml', 'text/xsl', 'text/xml'),
    'mpeg'  =>  'video/mpeg',
    'mpg' =>  'video/mpeg',
    'mpe' =>  'video/mpeg',
    'qt'  =>  'video/quicktime',
    'mov' =>  'video/quicktime',
    'avi' =>  array('video/x-msvideo', 'video/msvideo', 'video/avi', 'application/x-troff-msvideo'),
    'movie' =>  'video/x-sgi-movie',
    'doc' =>  array('application/msword', 'application/vnd.ms-office'),
    'docx'  =>  array('application/vnd.openxmlformats-officedocument.wordprocessingml.document', 'application/zip', 'application/msword', 'application/x-zip'),
    'dot' =>  array('application/msword', 'application/vnd.ms-office'),
    'dotx'  =>  array('application/vnd.openxmlformats-officedocument.wordprocessingml.document', 'application/zip', 'application/msword'),
    'xlsx'  =>  array('application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', 'application/zip', 'application/vnd.ms-excel', 'application/msword', 'application/x-zip'),
    'word'  =>  array('application/msword', 'application/octet-stream'),
    'xl'  =>  'application/excel',
    'eml' =>  'message/rfc822',
    'json'  =>  array('application/json', 'text/json'),
    'pem'   =>  array('application/x-x509-user-cert', 'application/x-pem-file', 'application/octet-stream'),
    'p10'   =>  array('application/x-pkcs10', 'application/pkcs10'),
    'p12'   =>  'application/x-pkcs12',
    'p7a'   =>  'application/x-pkcs7-signature',
    'p7c'   =>  array('application/pkcs7-mime', 'application/x-pkcs7-mime'),
    'p7m'   =>  array('application/pkcs7-mime', 'application/x-pkcs7-mime'),
    'p7r'   =>  'application/x-pkcs7-certreqresp',
    'p7s'   =>  'application/pkcs7-signature',
    'crt'   =>  array('application/x-x509-ca-cert', 'application/x-x509-user-cert', 'application/pkix-cert'),
    'crl'   =>  array('application/pkix-crl', 'application/pkcs-crl'),
    'der'   =>  'application/x-x509-ca-cert',
    'kdb'   =>  'application/octet-stream',
    'pgp'   =>  'application/pgp',
    'gpg'   =>  'application/gpg-keys',
    'sst'   =>  'application/octet-stream',
    'csr'   =>  'application/octet-stream',
    'rsa'   =>  'application/x-pkcs7',
    'cer'   =>  array('application/pkix-cert', 'application/x-x509-ca-cert'),
    '3g2'   =>  'video/3gpp2',
    '3gp'   =>  array('video/3gp', 'video/3gpp'),
    'mp4'   =>  'video/mp4',
    'm4a'   =>  'audio/x-m4a',
    'f4v'   =>  'video/mp4',
    'webm'  =>  'video/webm',
    'aac'   =>  'audio/x-acc',
    'm4u'   =>  'application/vnd.mpegurl',
    'm3u'   =>  'text/plain',
    'xspf'  =>  'application/xspf+xml',
    'vlc'   =>  'application/videolan',
    'wmv'   =>  array('video/x-ms-wmv', 'video/x-ms-asf'),
    'au'    =>  'audio/x-au',
    'ac3'   =>  'audio/ac3',
    'flac'  =>  array('audio/x-flac', 'audio/flac'),
    'ogg'   =>  'audio/ogg',
    'kmz' =>  array('application/vnd.google-earth.kmz', 'application/zip', 'application/x-zip'),
    'kml' =>  array('application/vnd.google-earth.kml+xml', 'application/xml', 'text/xml'),
    'ics' =>  'text/calendar',
    'ical'  =>  'text/calendar',
    'zsh' =>  'text/x-scriptzsh',
    '7zip'  =>  array('application/x-compressed', 'application/x-zip-compressed', 'application/zip', 'multipart/x-zip'),
    'cdr' =>  array('application/cdr', 'application/coreldraw', 'application/x-cdr', 'application/x-coreldraw', 'image/cdr', 'image/x-cdr', 'zz-application/zz-winassoc-cdr'),
    'wma' =>  array('audio/x-ms-wma', 'video/x-ms-asf'),
    'jar' =>  array('application/java-archive', 'application/x-java-application', 'application/x-jar', 'application/x-compressed'),
    'svg' =>  array('image/svg+xml', 'application/xml', 'text/xml'),
    'vcf' =>  'text/x-vcard',
    'srt' =>  array('text/srt', 'text/plain'),
    'vtt' =>  array('text/vtt', 'text/plain'),
    'ico' =>  array('image/x-icon', 'image/x-ico', 'image/vnd.microsoft.icon')
  );
  $extension = strtolower($extension);
  return empty($extension) ? $map : (
    !array_key_exists($extension, $map) ? FALSE : (
      (is_array($map[$extension]) && !$all) ? $map[$extension][0] : $map[$extension]
    )
  );
}

/**
 * Get typical file extensions given MIME type.
 */
function mediatheque_get_media_extension_map($mimetype = NULL, $all = FALSE) {
  static $map = NULL;
  if (empty($map)) {
    // Flip the map, taking care of array entries and possible overlapping values.
    // Because `array_reduce()` doesn't accept keys, we first modify array entries
    // to add the extensions.
    $am = mediatheque_get_mimetype_map();
    $ak = array_keys($am);
    $ao = array_map(function($k, $v) {
      return array(
        'ext' => $k,
        'mime' => $v,
      );
    }, $ak, $am);
    // We're now ready to iterate on the modified map to have MIME types as keys
    // and extensions as values. Need to handle cases with multiple extensions or
    // multiple MIME types.
    $map = array_reduce($ao, function($carry, $item) {
      foreach ((array)$item['mime'] as $mime) {
        if (isset($carry[$mime])) {
          $carry[$mime] = (array)$carry[$mime];
          $carry[$mime][] = $item['ext'];
        }
        else {
          $carry[$mime] = $item['ext'];
        }
        return $carry;
      }
    }, array());
  }
  $mimetype = strtolower($mimetype);
  return empty($mimetype) ? $map : (
    !array_key_exists($mimetype, $map) ? FALSE : (
      (is_array($map[$mimetype]) && !$all) ? $map[$mimetype][0] : $map[$mimetype]
    )
  );
}
function mediatheque_get_media_extension($mimetype) {
  return mediatheque_get_media_extension_map($mimetype);
}

/**
 * Get language name given code.
 */
function mediatheque_get_language_map($code = NULL) {
  require_once './includes/locale.inc';
  $map = _locale_get_predefined_list();
  return empty($code) ? $map : @$map[$code][0];
}

/**
 * Get language code given name.
 */
function mediatheque_get_language_code($name = NULL) {
  static $map = NULL;
  if (empty($map)) {
    $av = mediatheque_get_language_map();
    $ak = array_keys($av);
    $ao = array_map(function($k, $v) {
      return array(
        'code' => $k,
        'name' => $v,
      );
    }, $ak, $av);
    $map = array_reduce($ao, function($carry, $item) {
      foreach ($item['name'] as $name) {
        $carry[strtolower($name)][] = $item['code'];
      }
      return $carry;
    }, array());
  }
  $name = strtolower($name);
  return empty($name) ? $map : @$map[$name][0];
}

/**
 * Get metadata formatter name given MIME type.
 */
function mediatheque_get_metadata_formatter($mimetype) {
  $type = mediatheque_get_media_type($mimetype);
  $registry = _content_type_info();
  return isset($registry['field types']['metadata']['formatters'][$type]) ? $type : 'default';
}

/**
 * Get a volume-relative path.
 */
function mediatheque_get_volume_relative_path($volume, $path) {
  return str_replace(Path::slash($volume->path), '', $path);
}

/**
 * Get volumes.
 */
function mediatheque_get_volumes($field = NULL) {
  $result = db_query("SELECT * FROM {mediatheque_volumes}");
  $volumes = array();
  while ($volume = db_fetch_object($result)) {
    $volumes[$volume->mvid] = $field ? $volume->$field : $volume;
  }
  return $volumes;
}

/**
 * Get a file's document node if it exists.
 */
function mediatheque_get_document_from_path($path) {
  foreach (mediatheque_get_volumes('path') as $volume) {
    if (strpos($path, $volume) === 0) {
      // found a matching volume
      $nid = db_result(db_query("SELECT nid FROM {content_type_document} WHERE field_path_value='%s'",
        str_replace(Path::slash($volume), '', $path)
      ));
      if ($nid) return node_load($nid);
    }
  }
  return FALSE;
}

/**
 * Get a file's document node if it is unmodified.
 */
function mediatheque_get_document_from_path_unmodified($path) {
  $doc = mediatheque_get_document_from_path($path);
  if ($doc && $doc->field_metadata[MEDIATHEQUE_MD5]['value'] === md5_file($path)) {
    return $doc;
  }
  return FALSE;
}

function mediatheque_get_document_from_signature($md5) {
  $nid = db_result(db_query("SELECT nid FROM {content_field_metadata} WHERE field_metadata_name = 'mediatheque:md5' AND field_metadata_value = '%s'", $md5));
  return $nid ? node_load($nid) : FALSE;
}

/**
 * Get plugins for a certain MIME type.
 */
function mediatheque_get_plugins($mimetype, $function = 'metadata') {
  $plugins = array();
  $result = db_query("SELECT * FROM {mediatheque_plugins} WHERE function='%s' AND active=1 ORDER BY weight ASC", $function);
  while ($plugin = db_fetch_array($result)) {
    if (empty($plugin['mimetype'])) {
      $plugin['mimetype'] = '*';
    }
    if (_mediatheque_match_mimetype($mimetype, $plugin['mimetype'])) {
      $plugins[] = $plugin;
    }
  }
  return $plugins;
}

/**
 * Deactivate a plugin and remove all its queued executions.
 */
function mediatheque_plugin_deactivate($plugin_class) {
  db_query("UPDATE {mediatheque_plugins} SET active = 0 WHERE plugin = '%s'", db_escape_string($plugin_class));
  db_query("DELETE {queue} WHERE name = 'mediatheque_plugins' AND data LIKE '%s'", '%' . _mediatheque_serialized_fragment('plugin_class', $plugin_class) . '%');
}

/**
 * Create a new job on a volume.
 */
function mediatheque_volume_create_job($volume, $update_only = FALSE) {
  drupal_queue_include();
  $queue_folders = DrupalQueue::get('mediatheque_folders');
  $queue_folders->createItem(array('path' => $volume->path, 'mvid' => $volume->mvid, 'update' => $update_only));

  drupal_set_message(t('Added volume !label to processing queue.', array('!label' => $volume->label)));
}

/**
 * Load functions for volume.
 */
function mediatheque_volume_load($mvid) {
  return db_fetch_object(db_query("SELECT * FROM {mediatheque_volumes} WHERE mvid = %d", $mvid));
}
function mediatheque_volume_load_by_label($label) {
  return db_fetch_object(db_query("SELECT * FROM {mediatheque_volumes} WHERE label = '%s'", $label));
}

/**
 * Load function for plugin.
 */
function mediatheque_plugin_load($mpid) {
  return db_fetch_object(db_query("SELECT * FROM {mediatheque_plugins} WHERE mpid = %d", $mpid));
}

/**
 * Get download path for media.
 */
function mediatheque_get_download_path($doc) {
  return 'node/' . $doc->nid . '/mediatheque/download';
}

/**
 * Get file path for media.
 */
function mediatheque_get_document_path($doc, $volume = NULL) {
  if (!$volume) $volume = mediatheque_volume_load($doc->mediatheque['mvid']);
  return Path::slash($volume->path) . $doc->field_path[0]['value'];
}

/**
 * Collect metadata values given metadata keys.
 */
function mediatheque_collect_metadata($doc, $keys) {
  $values = array();
  foreach ($doc->field_metadata as $metadata) {
    if (in_array($metadata['name'], $keys)) {
      $values[$metadata['name']] = $metadata['value'];
    }
  }
  return $values;
}

/**
 * Download a file.
 * @see file_transfer()
 */
function mediatheque_transfer($path, $headers) {
  if (ob_get_level()) {
    ob_end_clean();
  }

  // IE cannot download private files because it cannot store files downloaded
  // over https in the browser cache. The problem can be solved by sending
  // custom headers to IE. See http://support.microsoft.com/kb/323308/en-us
  if (isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] == 'on')) {
    drupal_set_header('Cache-Control: private');
    drupal_set_header('Pragma: private');
  }

  foreach ($headers as $header) {
    // To prevent HTTP header injection, we delete new lines that are
    // not followed by a space or a tab.
    // See http://www.w3.org/Protocols/rfc2616/rfc2616-sec4.html#sec4.2
    $header = preg_replace('/\r?\n(?!\t| )/', '', $header);
    drupal_set_header($header);
  }

  // Transfer file in 1024 byte chunks to save memory usage.
  if ($fd = fopen($path, 'rb')) {
    while (!feof($fd)) {
      print fread($fd, 1024);
    }
    fclose($fd);
  }
  else {
    drupal_not_found();
  }
  exit();
}

// ======================= HELPERS ============================================

/**
 * Match a given MIME type to a pattern of MIME types.
 * @see drupal_match_path()
 */
function _mediatheque_match_mimetype($mimetype, $patterns) {
  $regexp = '/^(' . preg_replace(
    array('/(\r\n?|\n)/', '/\\\\\*/'),
    array('|', '.*'),
    preg_quote($patterns, '/')
  ) . ')$/';
  return preg_match($regexp, $mimetype);
}

/**
 * Return a serialized name/value pair.
 */
function _mediatheque_serialized_fragment($name, $value) {
  $serialized = serialize(array($name => $value));
  $serialized = substr($serialized, 5, -1);
  return $serialized;
}

/**
 * Extract a year from a date string.
 */
function _mediatheque_extract_year($date) {
  $matches = array();
  if (preg_match('/\D?Y?(\d{4})\D?/', $date, $matches)) { // arabic audio id3 has 'Y' prefix
    return $matches[1];
  }
  return FALSE;
}

class Path {
  static function trim($path) {
    return rtrim($path, DIRECTORY_SEPARATOR);
  }
  static function slash($path) {
    return self::trim($path) . DIRECTORY_SEPARATOR;
  }
}
