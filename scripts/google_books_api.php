#!/usr/bin/php
<?php

// INITIALIZATION
$_SERVER['HTTP_HOST']       = 'default';
$_SERVER['REMOTE_ADDR']     = '127.0.0.1';
$_SERVER['SERVER_SOFTWARE'] = $_SERVER['HTTP_USER_AGENT'] = 'PHP CLI';
$_SERVER['REQUEST_METHOD']  = 'GET';
$_SERVER['QUERY_STRING']    = '';
$_SERVER['PHP_SELF']        = $_SERVER['REQUEST_URI'] = __FILE__;

chdir('../../../../..');
include('./includes/bootstrap.inc');
drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);

$result = drupal_http_request(
	url(
		'https://www.googleapis.com/books/v1/volumes', 
		array(
			'external' => TRUE,
			'query' => 	array(
				'q' => 'isbn:' . $argv[1],
				'key' => 'AIzaSyCyFuh3eUes7fmH4aGYd0Wt4VP6CToVkRM',
			),
		)
	)
);
$data = json_decode($result->data);
if ($result->code != 200) {
	echo $data->error->code . ': ' . $data->error->message . "\n";
	die;
}

if (!$data->totalItems) {
	echo "No data found for " . $argv[1] . "\n";
}

foreach ($data->items as $item) {
	var_dump($item->volumeInfo);
}

?>
