<?php

/**
 * Implementation of hook_drush_help().
 */
function mediatheque_drush_help($section) {
  switch ($section) {
    case 'drush:mediatheque-move':
      return dt('Move files to a physical volume and enqueue them for processing.');
    case 'drush:mediatheque-metadata':
      return dt('Print metadata for a physical file.');
  }
}

/**
 * Implementation of hook_drush_command().
 */
function mediatheque_drush_command() {
  $items['mediatheque-move'] = array(
    'callback' => 'drush_mediatheque_move',
    'description' => 'Move files to a physical volume and enqueue them for processing.',
    'arguments' => array(
      'path' => 'Path where files currently reside.',
      'glob' => 'Comma-separated extensions to move.',
      'volume' => 'Volume label to receive the files.',
    ),
  );
  $items['mediatheque-metadata'] = array(
    'callback' => 'drush_mediatheque_metadata',
    'description' => 'Print metadata for a physical file.',
    'arguments' => array(
      'path' => 'File\'s full path.',
    ),
  );
  return $items;
}

/**
 * Implementation of 'mediatheque-move' command.
 */
function drush_mediatheque_move($path = NULL, $glob = NULL, $volume = NULL) {
  // Parse arguments.
  if (empty($path)) {
    drush_set_error('MEDIATHEQUE_MISSING_PATH', dt('Please specify the path where files currently reside.'));
    return;
  }
  if (empty($glob)) {
    drush_set_error('MEDIATHEQUE_MISSING_GLOB', dt('Please specify the comma-separated extensions to move.'));
    return;
  }
  if (empty($volume)) {
    drush_set_error('MEDIATHEQUE_MISSING_VOLUME', dt('Please specify the volume label to receive the files.'));
    return;
  }
  $v = mediatheque_volume_load_by_label($volume);
  if (empty($v)) {
    drush_set_error('MEDIATHEQUE_UNKNOWN_VOLUME', dt('Unknown volume !volume. Please specify a valid volume.', array('!volume' => $volume)));
    return;
  }
  $destination = $v->path;

  // MAIN LOOP
  $anything = FALSE;
  $glob = Path::slash($path) . '*.{' . $glob . '}';
  foreach (glob($glob, GLOB_BRACE) as $filename) {
    $pathinfo = pathinfo($filename);
    $isbn = $pathinfo['filename'];
    if (preg_match('/^(97(8|9))?\d{9}(\d|X)$/', $isbn, $matches)) {
      $nid = db_result(db_query("
        SELECT nid FROM {content_field_metadata} WHERE field_metadata_value = '%s'
      ", 'ISBN:' . $isbn));
      if (!empty($nid)) {
        drush_print(dt('Found document !isbn in database. Skipping.', array('!isbn' => $isbn)));
      }
      else {
        drush_print(dt('Moving !isbn to destination.', array('!isbn' => $isbn)));
        chmod($filename, 0776);
        if (!rename($filename, $destination . '/' . $pathinfo['basename'])) {
          drush_set_error('MEDIATHEQUE_ERROR_MOVING', dt('Error moving !filename.', array('!filename' => $filename)));
        }
        else {
          $anything = TRUE;
        }
      }
    }
    else {
      drush_print(dt('File !isbn is not an ISBN. Moving without checking.', array('!isbn' => $isbn)));
      chmod($filename, 0776);
      if (!rename($filename, $destination . '/' . $pathinfo['basename'])) {
        drush_set_error('MEDIATHEQUE_ERROR_MOVING', dt('Error moving !filename.', array('!filename' => $filename)));
      }
      else {
        $anything = TRUE;
      }
    }
  }

  // ACTIVATE VOLUME UPDATE
  if ($anything) {
    mediatheque_volume_create_job($v, TRUE);
    drush_print(dt('Created job for volume !volume.', array('!volume' => $volume)));
  }
}

/**
 * Implementation of 'mediatheque-metadata' command.
 */
function drush_mediatheque_metadata($path = NULL) {
  // Parse arguments.
  if (empty($path)) {
    drush_set_error('MEDIATHEQUE_MISSING_PATH', dt('Please specify the file\'s full path.'));
    return;
  }
  if (!is_file($path)) {
    drush_set_error('MEDIATHEQUE_INVALID_PATH', dt('The path !path does not refer to an existing file.', array('!path' => $path)));
    return;
  }

  $metadata = array();
  $mimetype = mediatheque_get_mimetype($path);
  $md5 = md5_file($path);
  $metadata[MEDIATHEQUE_MIMETYPE] = array(
    'name' => 'mediatheque:mime-type',
    'value' => $mimetype,
  );
  $metadata[MEDIATHEQUE_MD5] = array(
    'name' => 'mediatheque:md5',
    'value' => $md5,
  );
  $metadata[MEDIATHEQUE_SIZE] = array(
    'name' => 'mediatheque:size',
    'value' => filesize($path),
  );
  $metadata[MEDIATHEQUE_TYPE] = array(
    'name' => 'mediatheque:type',
    'value' => mediatheque_get_media_type($mimetype),
  );

  $doc = mediatheque_get_document_from_path($path);
  if (!$doc) {
    drush_set_error('MEDIATHEQUE_MISSING_PATH', dt('Document not found for path !path.',
      array('!path' => $path)
    ));
    return;
  }
  foreach (mediatheque_get_plugins($mimetype, 'metadata') as $plugin_info) {
    $plugin_class = $plugin_info['plugin'];
    $plugin = new $plugin_class($path, $doc);
    if (!$plugin) {
      drush_set_error('MEDIATHEQUE_INVALID_PLUGIN', dt('Error creating plugin !plugin.',
        array('!plugin' => $plugin_class)
      ));
      continue;
    }
    drush_print(dt('Analyzing using !plugin...', array('!plugin' => $plugin_class)));
    $plugin_metadata = $plugin->getMetadata(FALSE);
    if (!empty($plugin_metadata)) {
      $prefix = $plugin->getPrefix();
      // Add new metadata.
      foreach ($plugin_metadata as $metadata_entry) {
        $metadata[] = array(
          'name' => $prefix . ':' . $metadata_entry['name'],
          'value' => $metadata_entry['value'],
        );
      }
    }
  }
  drush_print(print_r($metadata, TRUE));
}
