<?php

/**
 * Implementation of hook_flag_default_flags().
 */
function mediatheque_configuration_flag_default_flags() {
  $flags = array();
  // Exported flag: "Stars".
  $flags['star'] = array(
    'content_type' => 'node',
    'title' => 'Stars',
    'global' => '0',
    'types' => array(
      '0' => 'document',
    ),
    'flag_short' => 'Star this media',
    'flag_long' => '',
    'flag_message' => '',
    'unflag_short' => 'Unstar this media',
    'unflag_long' => '',
    'unflag_message' => '',
    'unflag_denied_text' => '',
    'link_type' => 'toggle',
    'roles' => array(
      'flag' => array(
        '0' => 2,
      ),
      'unflag' => array(
        '0' => 2,
      ),
    ),
    'weight' => 0,
    'show_on_page' => 1,
    'show_on_teaser' => 1,
    'show_on_form' => 0,
    'access_author' => '',
    'i18n' => 0,
    'module' => 'mediatheque_configuration',
    'locked' => array(
      '0' => 'name',
    ),
    'api_version' => 2,
  );
  return $flags;

}

/**
 * Implementation of hook_node_info().
 */
function mediatheque_configuration_node_info() {
  $items = array(
    'document' => array(
      'name' => t('Document'),
      'module' => 'features',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'has_body' => '1',
      'body_label' => t('Body'),
      'min_word_count' => '0',
      'help' => '',
    ),
  );
  return $items;
}

/**
 * Implementation of hook_views_api().
 */
function mediatheque_configuration_views_api() {
  return array(
    'api' => '3.0',
  );
}
