<?php

/**
 * Implementation of hook_content_default_fields().
 */
function mediatheque_configuration_content_default_fields() {
  $fields = array();

  // Exported field: field_metadata
  $fields['document-field_metadata'] = array(
    'field_name' => 'field_metadata',
    'type_name' => 'document',
    'display_settings' => array(
      'weight' => '-4',
      'parent' => '',
      'label' => array(
        'format' => 'above',
      ),
      'teaser' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'full' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '5' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '4' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '2' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '3' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'token' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'metadata',
    'required' => '0',
    'multiple' => '1',
    'module' => 'cck_metadata',
    'active' => '1',
    'widget' => array(
      'default_value' => array(
        '0' => array(
          'name' => '',
          'value' => '',
          '_error_element' => 'value',
        ),
      ),
      'default_value_php' => NULL,
      'label' => 'Metadata',
      'weight' => '-4',
      'description' => '',
      'type' => 'cck_metadata_widget',
      'module' => 'cck_metadata',
    ),
  );

  // Exported field: field_path
  $fields['document-field_path'] = array(
    'field_name' => 'field_path',
    'type_name' => 'document',
    'display_settings' => array(
      'weight' => '-3',
      'parent' => '',
      'label' => array(
        'format' => 'hidden',
      ),
      'teaser' => array(
        'format' => 'hidden',
        'exclude' => 1,
      ),
      'full' => array(
        'format' => 'hidden',
        'exclude' => 1,
      ),
      '5' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '4' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '2' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '3' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'token' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'text',
    'required' => '1',
    'multiple' => '0',
    'module' => 'text',
    'active' => '1',
    'text_processing' => '0',
    'max_length' => '',
    'allowed_values' => '',
    'allowed_values_php' => '',
    'widget' => array(
      'rows' => 5,
      'size' => '200',
      'default_value' => array(
        '0' => array(
          'value' => '',
          '_error_element' => 'default_value_widget][field_path][0][value',
        ),
      ),
      'default_value_php' => NULL,
      'label' => 'Path',
      'weight' => '-3',
      'description' => '',
      'type' => 'text_textfield',
      'module' => 'text',
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Metadata');
  t('Path');

  return $fields;
}
