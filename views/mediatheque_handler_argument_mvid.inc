<?php

class mediatheque_handler_argument_mvid extends views_handler_argument_numeric {
  /**
   * Override the behavior of title(). Get the title of the node.
   */
  function title_query() {
    $labels = array();
    $placeholders = implode(', ', array_fill(0, sizeof($this->value), '%d'));

    $result = db_query("SELECT v.label FROM {mediatheque_volumes} v WHERE v.mvid IN ($placeholders)", $this->value);
    while ($volume = db_fetch_object($result)) {
      $labels[] = check_plain($volume->label);
    }
    return $labels;
  }
}

