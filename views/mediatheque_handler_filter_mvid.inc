<?php

class mediatheque_handler_filter_mvid extends views_handler_filter_in_operator {
  function get_value_options() {
    if (isset($this->value_options)) {
      return;
    }

    $this->value_options = array();
    foreach (mediatheque_get_volumes() as $mvid => $volume) {
      $this->value_options[$mvid] = $volume->label;
    }
  }
}
